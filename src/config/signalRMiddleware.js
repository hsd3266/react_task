import {
    createStore
} from 'redux';

export default function configureStore(reducer, enhancer) {
    const store = createStore(
        reducer,
        enhancer,
    );
    return store;
}

