import React, { Component, Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { fetchProducts, getCategories } from "./Actions";
import { addToCart } from "../Cart/Actions";
import './productList.css';

function Products(props) {

    const [productList, setProductList] = useState([]);
    const [modalData, openModal] = useState(null);
    const [filterValue, setFilterValue] = useState(null);

    useEffect(() => {
        props.fetchProducts();
        props.getCategories()
    }, []);

    useEffect(() => {
        if (filterValue) {
            const updatedProducts = props.products.filter(product => product.category.toLowerCase().includes(filterValue.toLowerCase()));
            setProductList(updatedProducts);
        }
        else setProductList(props.products)

    }, [props.products, filterValue])

    const getCategories = (event) => setFilterValue(event.target.value)
    return (
        <div>
            {!productList ? (
                <div>Loading...</div>
            ) : (
                    <React.Fragment>

                        <div className="text-center">
                            <h1 className="text-primary m-4">Product Details</h1>
                        </div>
                        <Fragment>
                            <div className="col-sm-6">

                                <select name="cars" className="custom-select" onClick={(value) => getCategories(value)}>
                                    <option selected value= "">Select Category</option>
                                    {
                                        props.categories && Array.isArray(props.categories) && props.categories.map((category, index) => {
                                            return <option key={index}>{category}</option>
                                        })
                                    }

                                </select>
                            </div>
                        </Fragment>


                        <div className="row">
                            {productList.map((product, index) => (

                                <div key={index} className="col-sm-12 col-md-4 col-lg-4">
                                    <div key={product.id} className="card text-center m-2" >
                                        <div className="card-body " >
                                            <div className="product products">
                                                <a
                                                    href={"#" + product.id}
                                                    onClick={() => openModal(product)}
                                                    data-toggle="modal" data-target="#myModal"
                                                >
                                                    <img src={product.image} alt={product.title}></img>
                                                    <p className="mt-2" >{product.title}</p>
                                                </a>

                                            </div>
                                            <div className="product-price">
                                                <button onClick={() => props.addToCart(product)}
                                                    className="btn btn-outline-primary form-control">
                                                    Add To Cart </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </React.Fragment>
                )}
            {modalData && (
                <div className="modal" id="myModal">
                    <div className="modal-dialog">
                        <div className="modal-content">

                            <div className="modal-header">
                                <h5 className="modal-title">{modalData.title}</h5>
                                <button type="button" className="close" onClick={() => openModal(null)} data-dismiss="modal">&times;</button>
                            </div>

                            <div className="modal-body">
                                {modalData.description}
                            </div>

                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" onClick={() => {
                                    props.addToCart(modalData);
                                    openModal(null);
                                }} data-dismiss="modal">Add To Cart</button>
                            </div>

                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}


const mapStateToProps = state => {
    return {
        products: state.productState.data,
        categories: state.productState.categories,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {

    return bindActionCreators(
        {
            fetchProducts,
            addToCart,
            getCategories
        }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Products);