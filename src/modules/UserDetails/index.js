import React, { Component } from 'react'
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import * as Constants from '../../Routes';
import { getUserDetails } from './Actions';
import { bindActionCreators } from 'redux';
import './userDetails.css';

class UserDetails extends Component {
    componentDidMount() {
        const userId = this.props.match.params.id;
        this.props.getUserDetails(userId);
    }
    render() {
        return (
            <div className="row mt-200">
                <div className="col-sm-12 col-md-4 col-lg-4 mx-auto">
                    <div className="card" >
                        <div className="card-header">
                            <h3>User Details</h3>
                        </div>
                        <div className="card-body ">
                            {this.props.singleUserData ?
                                <React.Fragment>
                                    <h4 className="card-title">{this.props.singleUserData.name.firstname} {this.props.singleUserData.name.lastname}</h4>
                                    <h6>{this.props.singleUserData.email}</h6>
                                    <p className="card-text">{this.props.singleUserData.address.city}</p>
                                </React.Fragment>
                                : <h4>No data Found</h4>
                            }
                            <Link to={Constants.Admin} >See Profiles</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        singleUserData: state.singleUserDataState.singleData,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {

    return bindActionCreators(
        {
            getUserDetails
        }, dispatch);
}

// const connectedUserDetailsPage = connect(mapStateToProps, mapDispatchToProps)(UserDetails);
// export { connectedUserDetailsPage as UserDetails };

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);