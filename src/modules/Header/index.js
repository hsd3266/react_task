import { Fragment, React } from 'react';
import { withRouter } from 'react-router-dom'

function header(props) {

    const logout = () => {
        localStorage.clear();
        props.history.push('/login');
    }

    return (
        <Fragment>
            {
                <div className="float-right mr-5">
                    <h1 className="text-danger">
                        <i className="fa fa-sign-out" onClick={() => logout()}></i>
                    </h1>
                </div>
            }
            {props.children}
        </Fragment>

    )
}

export default withRouter(header);
