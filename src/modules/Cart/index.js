import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { removeFromCart } from "./Actions";
import './cart.css'

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            address: "",
        };
    }
    closeModal = () => {
        this.props.clearOrder();
    };
    render() {
        const { cartItems } = this.props;
        return (
            <div>
                {cartItems.length === 0 ? (
                    <div className="cart m-3 ">
                        <div className="cart-header">
                            <strong className="text-danger"> Cart is empty </strong>
                        </div>
                    </div>
                ) : (
                        <Fragment>
                            <div onClick={() => this.props.history.goBack()}>
                                Back
                            </div>
                            <div className="cart m-3 ">
                                <div className="cart-header">
                                    <strong className="text-primary">You have {cartItems.length} in the cart{" "}</strong>
                                </div>

                            </div>
                        </Fragment>

                    )}
                <div>
                    <div className="cart">
                        <ul className="cart-items">
                            {cartItems.map((item) => (
                                <li key={item.id}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-4 co-lg-4 mx-auto">
                                            <img width="100%" height="400px" src={item.image} alt={item.title}></img>

                                            <p className="mt-1">{item.title}</p>
                                            <div className="float-right">
                                                <button
                                                    className="btn"
                                                    onClick={() => this.props.removeFromCart(item)}
                                                >
                                                    <i className="fa fa-trash-o" ></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div>

                                        <div className="right">

                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>

                </div>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        cartItems: state.cartState.cartItems,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {

    return bindActionCreators(
        {
            removeFromCart,
        }, dispatch);
}

// const connectedCart = connect(mapStateToProps, mapDispatchToProps)(Cart);
// export { connectedCart as Cart };

export default connect(mapStateToProps, mapDispatchToProps)(Cart);